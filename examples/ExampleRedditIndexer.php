#!/usr/local/bin/php
<?php
/**
 * Run me with:
 *      php ExampleRedditIndexer.php
 */
 
// Load composer deps
require __DIR__ . '/../vendor/autoload.php';

// Load environment vars
if (file_exists(dirname(__DIR__) . '/.env.test')) {
    (new \Dotenv\Dotenv(dirname(__DIR__), '.env.test'))->load();
} else {
    echo "\nWARNING: create a .env.test file! (see .env.test.example)\n";
}
 

echo "Example Reddit Indexer\n";
echo "--\n";
echo "This script will index posts from 5 subreddits every 15 seconds\n";
echo "and post a message when new posts are found\n";
echo "--\n";
echo "Use Ctrl-C to exit.\n";
echo "--\n";

/**
 * These are our subreddits and type hints.
 */
$subreddits = [
    [
        'name' => 'ass',
        'type_hints' => [
            'fixed_gender_type' => 'solo-women',
            'fixed_artstyle_type' => 'real-life',
        ],
    ],
    [
        'name' => 'gonewild',
        'type_hints' => [
            'fixed_artstyle_type' => 'real-life',
        ],
    ],
    [
        'name' => 'adorableporn',
        'type_hints' => [
            'always_involve_women' => true,
            'never_gay' => true,
            'never_transexual' => true,
            'fixed_artstyle_type' => 'real-life',
        ],
    ],
    [
        'name' => 'nsfw_gif',
        'type_hints' => [
            'fixed_artstyle_type' => 'real-life',
        ],
    ],
    [
        'name' => 'rule34',
        'type_hints' => [
            'bans_reallife' => true,
            'allows_hentai' => true,
            'allows_3d' => true,
        ],
    ],
];

/** 
 * Creating a config for each sub we want to index.
 * Normally you'd generate these from your subs and type hints in a DB.
 */
$sources = [];

foreach ($subreddits as $subreddit) {
    // The source config determines how posts are indexed
    $sourceConfig = new \AIFap\Indexer\Data\SourceConfig(
        'reddit-new-posts', // Indexing method
        new \AIFap\Indexer\Data\TypeHints($subreddit['type_hints'])
    );
    $sourceConfig->setSourceInfo('subreddit_slug', $subreddit['name']);
    $sourceConfig->setSourceInfo('limit', 100);
    $sourceConfig->setSourceInfo('user_agent', 'AIF-Testing');
    $sourceConfig->setSourceInfo('gfycat_client_id', getenv('GFYCAT_CLIENT_ID'));
    $sourceConfig->setSourceInfo('gfycat_client_secret', getenv('GFYCAT_CLIENT_SECRET'));
    
    $sources[] = [
        'sourceConfig' => $sourceConfig,
        'subreddit' => $subreddit,
    ];
}


/**
 * Initial resume information for each source. 
 * Normally you'd save this info to the DB in between indexing runs.
 */
$resumeStorage = [];
foreach ($subreddits as $subreddit) {
    $resumeStorage[$subreddit['name']] = [];
}
    
    
/**
 * This is a loop with a sleep, but normally you'd call your indexer on a cron etc.
 */
$firstRun = true;
echo "Initial indexing run: (might take a minute or two)\n";
$indexer = new AIFap\Indexer\IndexingService;
while (true) {
    foreach ($sources as $source) {
        $sourceConfig = $source['sourceConfig'];
        
        // Get resume info from storage (normally DB)
        $resumeInfo = $resumeStorage[$source['subreddit']['name']];
        
        echo "Checking /r/" . $source['subreddit']['name'] . ":\n";
        
        // Set resume info so indexer knows where to start
        $sourceConfig->setResumeInfo($resumeInfo);
            
        // Perform indexing
        $result = $indexer->index($source['sourceConfig']);
        
        // Do something with results
        if (!$firstRun) {
            foreach ($result->postData as $postData) {
                echo "  + New post from /r/" . $source['subreddit']['name'] . ": $postData->title\n";
            }
        }
        
        foreach ($result->warnings as $warning) {
            echo "  WARNING: $warning\n";
        }
        
        echo "  Indexed $result->postCount posts from /r/" . $source['subreddit']['name'] . "\n";
        
        // Store resume info for next run
        $resumeStorage[$source['subreddit']['name']] = $result->resumeInfo;
        
       // var_dump($result->resumeInfo);
    }
    
    // Wait for next run
    echo "Waiting for 15 seconds...\n";
    sleep(15);
    
    if ($firstRun) {
        $firstRun = false;
        echo "Resuming indexing: (should be quick)\n";
    }
}