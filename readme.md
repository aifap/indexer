## AIFap Indexer v2

This is the indexing module which provides content for aifap.net. 

### Example implementation

Included is an example of a simpler indexer that continuously scans a handful of 
subreddits for new posts. Copy .test.env.example to .test.env and fill in the needed environment variables.
Then run `php examples/ExampleRedditIndexer.php`.

Check the source code for `ExampleRedditIndexer.php` to learn how it 
works.

```
$ php examples/ExampleRedditIndexer.php
 
Example Reddit Indexer
--
This script will index posts from 5 subreddits every 15 seconds
and post a message when new posts are found
--
Use Ctrl-C to exit.
--
Initial indexing run: (might take a minute or two)
Checking /r/ass:
  WARNING: No previous state info - this is an inital indexing.
  Indexed 100 posts from /r/ass
Checking /r/gonewild:
  WARNING: No previous state info - this is an inital indexing.
  Indexed 100 posts from /r/gonewild
Checking /r/adorableporn:
  WARNING: No previous state info - this is an inital indexing.
  Indexed 100 posts from /r/adorableporn
Checking /r/nsfw_gif:
  WARNING: No previous state info - this is an inital indexing.
  Indexed 100 posts from /r/nsfw_gif
Checking /r/rule34:
  WARNING: No previous state info - this is an inital indexing.
  Indexed 99 posts from /r/rule34
Waiting for 15 seconds...

Resuming indexing: (should be quick)
Checking /r/ass:
  Indexed 0 posts from /r/ass
Checking /r/gonewild:
  + New post from /r/gonewild: Going to apologize to my [F]riends in advance for being late tonight.....I got side tracked!
  + New post from /r/gonewild: [M28] new here looking for some comments or pms
  Indexed 2 posts from /r/gonewild
Checking /r/adorableporn:
  Indexed 0 posts from /r/adorableporn
Checking /r/nsfw_gif:
  Indexed 0 posts from /r/nsfw_gif
Checking /r/rule34:
  Indexed 0 posts from /r/rule34
Waiting for 15 seconds...

Checking /r/ass:
  Indexed 0 posts from /r/ass
Checking /r/gonewild:
  + New post from /r/gonewild: I love this picture (f)
  Indexed 1 posts from /r/gonewild
Checking /r/adorableporn:
  Indexed 0 posts from /r/adorableporn
Checking /r/nsfw_gif:
  Indexed 0 posts from /r/nsfw_gif
Checking /r/rule34:
  Indexed 0 posts from /r/rule34
Waiting for 15 seconds...
```


### Usage 

```php
use AIFap\Indexer\Data\TypeConstants;
use AIFap\Indexer\Data\SourceConfig;
use AIFap\Indexer\Data\TypeHints;
use AIFap\Indexer\IndexingService;

// Create a config object which specifies the type of indexing to be done
// and provides hints about the types of post available.
$sourceConfig = new SourceConfig('reddit-new-posts', new TypeHints([
    'always_solo' => true,
    'fixed_artstyle_type' => TypeConstants::ARTSTYLE_REALLIFE,
]));

// Configure the indexing source (depends on type of source)
$sourceConfig->setSourceInfo('subreddit_slug', 'gonewild');
$sourceConfig->setSourceInfo('user_agent', 'AIF-Testing');
$sourceConfig->setSourceInfo('limit', 3);

// Create and run indexer
$service = new IndexingService;
$result = $service->index($sourceConfig);
```

IndexingService::index() can throw an exception if source info parameters are
missing.

Using the indexing results:

```php
// $result is an IndexingResult object
$result->sourceConfig; // Your original source config object

$result->success; // true if posts could be indexed
$result->warnings; // array of warning messages

$result->postCount; // number of indexed posts
$result->postData; // array of post data objects
/*
[
    {
        "identifier": "t3_abcdef",
        "permalink": "https://reddit.com/r/gonewild/abcdef",
        "title": "...",
        "mediaUrl": "https://i.reddit.com/abcdef.jpg",
        "typeGender": "solo-women",
        "typeMedia": "image",
        "typeArtstyle": "reallife",
        ...
    },
    ...
]
*/


$result->canIndexMore; // true if you can resume immediately and index more posts.
$result->resumeInfo; // State used to resume indexing


```

Resuming indexing from where you left off:
    
```php
// First index is the same as before
$sourceConfig = new SourceConfig('reddit-new-posts', new TypeHints([
    'always_solo' => true,
    'fixed_artstyle_type' => TypeConstants::ARTSTYLE_REALLIFE,
]));
$sourceConfig->setSourceInfo('subreddit_slug', 'gonewild');
$sourceConfig->setSourceInfo('user_agent', 'AIF-Testing');
$sourceConfig->setSourceInfo('limit', 3);

$service = new IndexingService;
$result = $service->index($sourceConfig);

// Do something with your results

// Store resumeInfo somewhere
$resumeInfo = $result->resumeInfo;

//
// ...
//

// Resuming:
$sourceConfig = new SourceConfig('reddit-new-posts', new TypeHints([
    'always_solo' => true,
    'fixed_artstyle_type' => TypeConstants::ARTSTYLE_REALLIFE,
]));
$sourceConfig->setSourceInfo('subreddit_slug', 'gonewild');
$sourceConfig->setSourceInfo('user_agent', 'AIF-Testing');
$sourceConfig->setSourceInfo('limit', 3);

// To resume indexing, provide the resume info:
$sourceConfig->setResumeInfo($resumeInfo);

$service = new IndexingService;
$result = $service->index($sourceConfig);
```

Resume info can be safely JSON encoded and stored in a database (use a TEXT field or larger).


### Testing
There are some PHPUnit tests under ./tests. They're not great at the moment.

Copy .env.test.example to .env.test and set the required environment variables 
before running phpunit.


### Adding new sources

New sources can be added by extending AIFap\Indexer\Sources\Source. See the 
RedditNewPostsSource for an example. Remember to define a constant SOURCE_KEY.

Once your source is created, add it to the list in IndexingService or call
IndexingService::registerSource().

### Adding new processing/clean-up steps

New clean-up steps can be added by extending AIFap\Indexer\Processing\PostProcessor.

Once the new processor is created, add it to the list in IndexingService or call
IndexingService::registerProcessor().