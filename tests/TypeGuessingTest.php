<?php
if (file_exists(dirname(__DIR__) . '/.env.test')) {
    (new \Dotenv\Dotenv(dirname(__DIR__), '.env.test'))->load();
} else {
    echo "\nWARNING: create a .env.test file! (see .env.test.example)\n";
}

use PHPUnit\Framework\TestCase as TestCase;

use AIFap\Indexer\Data\TypeConstants;
use AIFap\Indexer\Data\PostData;
use AIFap\Indexer\Data\TypeHints;
use AIFap\Indexer\Data\SourceConfig;

use AIFap\Indexer\Categorization\ArtstyleTypeGuesser;
use AIFap\Indexer\Categorization\GenderTypeGuesser;
use AIFap\Indexer\Categorization\MediaTypeGuesser;

final class TypeGuessingTest extends TestCase {
    public function testCanGuessArtstyleFixed() {
        $guesser = new ArtstyleTypeGuesser;
        
        $postData = new PostData;
        $postData->title = '[f]emale with [m]ale';
        
        $sourceConfig = new SourceConfig('test', new TypeHints([
            'fixed_artstyle_type' => TypeConstants::ARTSTYLE_3D,
        ]));
        
        $postData = new PostData;
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::ARTSTYLE_3D);
    }
    
    public function testCanGuessArtstyleFromAllows() {
        $guesser = new ArtstyleTypeGuesser;
        
        $postData = new PostData;
        $postData->title = '[f]emale with [m]ale';
        $sourceConfig = new SourceConfig('test', new TypeHints([
            'allows_hentai' => true,
        ]));
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::ARTSTYLE_HENTAI);
        
        $sourceConfig = new SourceConfig('test', new TypeHints([
            'allows_reallife' => true,
            'allows_3d' => true,
        ]));
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::ARTSTYLE_REALLIFE);
    }
    
    public function testCanGuessMediaFiletypes() {
        $guesser = new MediaTypeGuesser;
        
        $postData = new PostData;
        $postData->title = '???';
        
        $sourceConfig = new SourceConfig('test', new TypeHints([
        ]));
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::MEDIA_UNKNOWN);
        
        $postData = new PostData;
        $postData->title = '???';
        $postData->mediaUrl = 'https://example.com/test.gif';
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::MEDIA_ANIMATED);
        
        $postData = new PostData;
        $postData->title = '???';
        $postData->mediaUrl = 'https://example.com/test.png';
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::MEDIA_IMAGE);
    }
    
    public function testCanGuessMediaHosts() {
        $guesser = new MediaTypeGuesser;
        
        $sourceConfig = new SourceConfig('test', new TypeHints([
        ]));
        $postData = new PostData;
        $postData->title = '???';
        $postData->mediaUrl = 'https://pornhub.com/test';
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::MEDIA_VIDEO);
        
        $postData = new PostData;
        $postData->title = '???';
        $postData->mediaUrl = 'https://imgur.com/foo';
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::MEDIA_IMAGE);
        
        $postData = new PostData;
        $postData->title = '???';
        $postData->mediaUrl = 'https://imgur.com/foo.mp4';
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::MEDIA_ANIMATED);
    }
    
    public function testCanGuessMediaFromSourceHint() {
        $guesser = new MediaTypeGuesser;
        
        $postData = new PostData;
        $postData->title = '???';
        $postData->mediaUrl = 'https://example.com/foo';
        $postData->sourceMediaTypeHint = 'rich:video';
        
        $sourceConfig = new SourceConfig(\AIFap\Indexer\Sources\RedditNewPostsSource::SOURCE_KEY, new TypeHints([
        ]));
        
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::MEDIA_VIDEO);
    }
    
    public function testCanGuessGenderTitleHints() {
        $guesser = new GenderTypeGuesser;
        
        $postData = new PostData;
        $postData->title = '[f]emale with [m]ale';
        
        $sourceConfig = new SourceConfig('test', new TypeHints([
        ]));
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::GENDER_STRAIGHT);
        
        $postData = new PostData;
        $postData->title = '[f20]emale';
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::GENDER_SOLO_WOMEN);
        
        $postData = new PostData;
        $postData->title = '{t}ransexual';
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::GENDER_TRANSEXUAL);
    }
    
    public function testCanGuessGenderGroups() {
        $guesser = new GenderTypeGuesser;
        $postData = new PostData;
        $postData->title = 'threesome';
        
        $sourceConfig = new SourceConfig('test', new TypeHints([
            'always_gay' => true,
        ]));
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::GENDER_GAY);
        
        $postData = new PostData;
        $postData->title = '[m]ale';
        
        $sourceConfig = new SourceConfig('test', new TypeHints([
        ]));
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::GENDER_SOLO_MEN);
        
        $postData = new PostData;
        $postData->title = '???';
        
        $sourceConfig = new SourceConfig('test', new TypeHints([
            'always_involve_men' => true,
            'always_gay' => true,
        ]));
        
        $guess = $guesser->guess($postData, $sourceConfig);
        $this->assertEquals($guess, TypeConstants::GENDER_GAY);
    }
}
