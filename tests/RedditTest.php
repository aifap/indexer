<?php
if (file_exists(dirname(__DIR__) . '/.env.test')) {
    (new \Dotenv\Dotenv(dirname(__DIR__), '.env.test'))->load();
} else {
    echo "\nWARNING: create a .env.test file! (see .env.test.example)\n";
}

use PHPUnit\Framework\TestCase as TestCase;

use AIFap\Indexer\External\Reddit;

final class RedditTest extends TestCase {
    public function testCanCreate() {
        $reddit = new Reddit(getenv('REDDIT_USER_AGENT'));
        $this->assertNotNull($reddit);
    }
    
    public function testCanGetPost() {
        $reddit = new Reddit(getenv('REDDIT_USER_AGENT'));
        
        $redditPost = $reddit->viewPost(getenv('REDDIT_EXAMPLE_POST_FULLNAME'));
        $this->assertNotNull($redditPost);
        $this->assertEquals($redditPost->title, getenv('REDDIT_EXAMPLE_POST_TITLE'));
    }
    
    public function testCanGetPage() {
        $reddit = new Reddit(getenv('REDDIT_USER_AGENT'));
        
        $redditPost = $reddit->viewPost(getenv('REDDIT_EXAMPLE_POST_FULLNAME'));
        
        $redditPage = $reddit->indexNew(getenv('REDDIT_EXAMPLE_SUB'));
        $this->assertNotNull($redditPage);
        
        $redditPage = $reddit->indexNew(getenv('REDDIT_EXAMPLE_SUB'), [
            'before' => $redditPost->name,
            'limit' => 3,
        ]);
        $this->assertNotNull($redditPage);
        $this->assertEquals(3, count($redditPage->children));
    }
}


