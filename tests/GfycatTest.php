<?php
if (file_exists(dirname(__DIR__) . '/.env.test')) {
    (new \Dotenv\Dotenv(dirname(__DIR__), '.env.test'))->load();
} else {
    echo "\nWARNING: create a .env.test file! (see .env.test.example)\n";
}

use PHPUnit\Framework\TestCase as TestCase;

use AIFap\Indexer\External\Gfycat;

final class GfycatTest extends TestCase {
    public function testCanCreate() {
        $gfy = new Gfycat(getenv('GFYCAT_CLIENT_ID'), getenv('GFYCAT_CLIENT_SECRET'));
        $this->assertNotNull($gfy);
    }
    
    public function testCanGetGfycat() {
        $gfy = new Gfycat(getenv('GFYCAT_CLIENT_ID'), getenv('GFYCAT_CLIENT_SECRET'));
        $this->assertNotNull($gfy);
        
        $gfyItem = $gfy->viewGfycat(getenv('GFYCAT_EXAMPLE_POST_ID'));
        $this->assertNotNull($gfyItem);
        $this->assertEquals($gfyItem->title, 'Candy cane surprise');
    }
}


