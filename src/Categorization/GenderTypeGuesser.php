<?php
namespace AIFap\Indexer\Categorization;

use AIFap\Indexer\Data\TypeConstants;
use AIFap\Indexer\Data\PostData;
use AIFap\Indexer\Data\SourceConfig;

class GenderTypeGuesser {
    static $titleStringsMale = [
        '/\[\d* ?m ?\d*\]/i', 
        '/\(\d* ?m ?\d*\)/i', 
        '/\{\d* ?m ?\d*\}/i', 
    ];
    static $titleStringsFemale = [
        '/\[\d* ?f ?\d*\]/i', 
        '/\(\d* ?f ?\d*\)/i', 
        '/\{\d* ?f ?\d*\}/i', 
    ];
    static $titleStringsTrans = [
        '/\[\d* ?t ?\d*\]/i', 
        '/\(\d* ?t ?\d*\)/i', 
        '/\{\d* ?t ?\d*\}/i', 
    ];
    static $titleStringsSolo = [
    ];
    static $titleStringsNonsolo = [
        '/orgy/',
        '/threesome/',
        '/3some/'
    ];
    
    public function guess(PostData $post, SourceConfig $sourceConfig) {
        $hints = $sourceConfig->getTypeHints();
        
        // Source has a fixed gender type
        if ($hints && $hints->fixedGenderType) {
            return $hints->fixedGenderType;
        }
        
        // Trans gender type always wins out
        if ($hints && $hints->alwaysTransexual) {
            return TypeConstants::GENDER_TRANSEXUAL;
        }
        
        $definitelySolo = ($hints && $hints->alwaysSolo) || $this->titleStringMatch($post->title, static::$titleStringsSolo);
        $definitelyNonSolo = ($hints && $hints->neverSolo) || $this->titleStringMatch($post->title, static::$titleStringsNonsolo);
        
        // Probably solo, try to work out gender
        if ($definitelySolo  && !$definitelyNonSolo) {
            if (($hints && $hints->alwaysInvolveMen) || $this->titleStringMatch($post->title, static::$titleStringsMale)) {
                return TypeConstants::GENDER_SOLO_MEN;
            }
            if (($hints && $hints->alwaysInvolveWomen) || $this->titleStringMatch($post->title, static::$titleStringsFemale)) {
                return TypeConstants::GENDER_SOLO_WOMEN;
            }
        }
        
        // Probably not solo, try to work out orientation
        if ($definitelyNonSolo && !$definitelySolo) {
            if ($hints) {
                if ($hints->alwaysStraight) {
                    return TypeConstants::GENDER_STRAIGHT;
                }
                if ($hints->alwaysGay) {
                    return TypeConstants::GENDER_GAY;
                }
                if ($hints->alwaysLesbian) {
                    return TypeConstants::GENDER_LESBIAN;
                }
            }
        }
        
        // Don't know if this is solo or not --
        
        // Male and female genders in title -> straight
        if ($this->titleStringMatch($post->title, static::$titleStringsMale)
            && $this->titleStringMatch($post->title, static::$titleStringsFemale)) {
            return TypeConstants::GENDER_STRAIGHT;
        }
        
        // Only male genders in title -> solo men
        if ($this->titleStringMatch($post->title, static::$titleStringsMale)) {
            return TypeConstants::GENDER_SOLO_MEN;
        }
        // Only female genders in title -> solo men
        if ($this->titleStringMatch($post->title, static::$titleStringsFemale)) {
            return TypeConstants::GENDER_SOLO_WOMEN;
        }
        // Only trans genders in title -> transexual
        if ($this->titleStringMatch($post->title, static::$titleStringsTrans)) {
            if (!$hints || !$hints->neverTransexual) {
                return TypeConstants::GENDER_TRANSEXUAL;
            }
        }
        
        // No idea what's in this based on the title
        // Assume it's not solo
        if ($hints) {
            // Use orientation hint to work out gendertype
            if ($hints->alwaysGay) {
                return TypeConstants::GENDER_GAY;
            }
            if ($hints->alwaysLesbian) {
                return TypeConstants::GENDER_GAY;
            }
            if ($hints->alwaysStraight) {
                return TypeConstants::GENDER_STRAIGHT;
            }
        }
            
        // No orientation hint, can't work out a gender
        return TypeConstants::GENDER_UNKNOWN;
    }
    
    
    private function titleStringMatch($title, $strings) {
        $title = strtolower($title);
        foreach ($strings as $str) {
            if (preg_match($str, $title)) {
                return true;
            }
        }
        return false;
    }
}


