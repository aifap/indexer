<?php
namespace AIFap\Indexer\Categorization;

use AIFap\Indexer\Data\TypeConstants;
use AIFap\Indexer\Data\PostData;
use AIFap\Indexer\Data\SourceConfig;

class MediaTypeGuesser {
    public function guess(PostData $post, SourceConfig $sourceConfig) {
        $guess = null;
        if (!$guess) {
            $guess = $this->checkImgur($post->mediaUrl);
        }
        if (!$guess) {
            $guess = $this->checkImageHosts($post->mediaUrl);
        }
        if (!$guess) {
            $guess = $this->checkVideoHosts($post->mediaUrl);
        }
        if (!$guess) {
            $guess = $this->checkFileType($post->mediaUrl);
        }
        if (!$guess) {
            $guess = $this->checkSourceMediaTypeHint($post->sourceMediaTypeHint, $sourceConfig);
        }
        if (!$guess) {
            $guess = TypeConstants::MEDIA_UNKNOWN;
        }
        
        return $guess;
    }
    
    private function checkSourceMediaTypeHint($hint, $sourceConfig) {
        if ($hint) {
            $sourceKey = $sourceConfig->getSourceKey();
            $redditSources = [
                \AIFap\Indexer\Sources\RedditNewPostsSource::SOURCE_KEY,
            ];
            
            if (in_array($sourceKey, $redditSources)) {
                if ($hint == 'image') {
                    return TypeConstants::MEDIA_IMAGE;
                }
                if ($hint == 'rich:video') {
                    return TypeConstants::MEDIA_VIDEO;
                }
            }
        }
    }
    
    private function checkFileType($url) {
        if (strpos($url, '.gif') !== false) {
            return TypeConstants::MEDIA_ANIMATED;
        }
        if (strpos($url, '.mp4') !== false) {
            return TypeConstants::MEDIA_ANIMATED;
        }
        if (strpos($url, '.png') !== false
            || strpos($url, '.jpg') !== false
            || strpos($url, '.jpeg') !== false) {
            return TypeConstants::MEDIA_IMAGE;
        }
        return null;
    }
    
    private function checkImageHosts($url) {
        if (strpos($url, 'gfycat.com/') !== false) {
            return TypeConstants::MEDIA_ANIMATED;
        }
        return null;
    }
    
    private function checkImgur($url) {
        if (strpos($url, 'https://imgur.com/a/') === 0
            || strpos($url, 'http://imgur.com/a/') === 0) {
            // TODO: imgur album support
            return TypeConstants::MEDIA_UNKNOWN;
        }
        if (strpos($url, 'https://i.imgur.com/') === 0
            || strpos($url, 'http://i.imgur.com/') === 0) {
            if (strpos($url, '.gif') !== false) {
                return TypeConstants::MEDIA_ANIMATED;
            }
            if (strpos($url, '.mp4') !== false) {
                return TypeConstants::MEDIA_ANIMATED;
            }
            return TypeConstants::MEDIA_IMAGE;
        }
        if (strpos($url, 'https://imgur.com/') === 0
            || strpos($url, 'http://imgur.com/') === 0) {
            if (strpos($url, '.gif') !== false) {
                return TypeConstants::MEDIA_ANIMATED;
            }
            if (strpos($url, '.mp4') !== false) {
                return TypeConstants::MEDIA_ANIMATED;
            }
            return TypeConstants::MEDIA_IMAGE;
        }
        return null;
    }
    
    public function checkVideoHosts($url) {
        if (strpos($url, 'pornhub.com/') !== false) {
            return TypeConstants::MEDIA_VIDEO;
        }
        if (strpos($url, 'youtube.com/') !== false) {
            return TypeConstants::MEDIA_VIDEO;
        }
        if (strpos($url, 'youtu.be') !== false) {
            return TypeConstants::MEDIA_VIDEO;
        }
        if (strpos($url, 'xvideos.com') !== false) {
            return TypeConstants::MEDIA_VIDEO;
        }
        if (strpos($url, 'xhamster.com') !== false) {
            return TypeConstants::MEDIA_VIDEO;
        }
        return null;
    }
}