<?php
namespace AIFap\Indexer\Categorization;

use AIFap\Indexer\Data\SourceConfig;
use AIFap\Indexer\Data\PostData;
use AIFap\Indexer\Data\TypeConstants;

/**
 * Attempts to guess the artstyle-type of a post.
 */
class ArtstyleTypeGuesser {
    /**
     * Attempts to determine the post's artstyle type
     */
    public function guess(PostData $postData, SourceConfig $sourceConfig) {
        $hints = $sourceConfig->getTypeHints();
        
        // TODO: improve type guessing
        if ($hints) {
            if ($hints->fixedArtstyleType) {
                return $hints->fixedArtstyleType;
            }
            if ($hints->allowsReallife) {
                return TypeConstants::ARTSTYLE_REALLIFE;
            }
            if ($hints->allowsHentai) {
                return TypeConstants::ARTSTYLE_HENTAI;
            }
            if ($hints->allows3d) {
                return TypeConstants::ARTSTYLE_3D;
            }
        }
        return TypeConstants::ARTSTYLE_REALLIFE;
    }
}
