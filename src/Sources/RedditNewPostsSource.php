<?php
namespace AIFap\Indexer\Sources;

use Carbon\Carbon;

use AIFap\Indexer\Data\PostData;
use AIFap\Indexer\Data\IndexingResult;
use AIFap\Indexer\Sources\Source;
use AIFap\Indexer\External\Reddit;

/**
 * Grabs new posts from a subreddit.
 */
class RedditNewPostsSource extends Source {
    const SOURCE_KEY = 'reddit-new-posts';
    
    const MAX_PAGE_SIZE = 100;
    const RESUME_POSTS_COUNT = 10;
    
    protected $reddit;
    protected $subredditSlug;
    protected $afterPosts = [];
    protected $limit = 100;
    
    public function setup() {
        $this->subredditSlug = $this->config->getSourceInfo('subreddit_slug', ['required' => true]);
        $this->limit = $this->config->getSourceInfo('limit', ['default' => self::MAX_PAGE_SIZE]);
        
        $this->afterPosts = $this->config->getResumeInfo('after_posts');
        
        $this->reddit = new Reddit($this->config->getSourceInfo('user_agent', ['required' => true]));
    }
    
    public function run() {
        $result = new IndexingResult($this->config);
        
        /**
         * Attempt to get the last post we indexed so it can be used for pagination.
         */
        $lastPost = null;
        if (count($this->afterPosts) > 0) {
            $lastPost = $this->findLastExistingPost();
            if (!$lastPost) {
                $result->addWarning('Could not retrieve previous indexed posts. Discarding previous state and doing an initial indexing.');
            }
        } else {
            $result->addWarning('No previous state info - this is an inital indexing.');
        }
        
        /**
         * Grab pages of posts
         */
        $pages = [];
        $remainingLimit = $this->limit;
        $beforeFullname = null;
        if ($lastPost) {
            $beforeFullname = $lastPost->name;
        }
        
        $result->canIndexMore = false;
        while ($remainingLimit > 0) {
            $page = $this->indexPage($beforeFullname, $remainingLimit);
            if (!$page) {
                break;
            }
            
            $count = count($page->children);
            if ($count == 0) {
                // Reached the end of the feed
                $result->canIndexMore = true;
                break;
            }
            
            $pages[] = $page;
            
            if ($page->before) {
                $remainingLimit -= $count;
                $beforeFullname = $page->before;
            } else {
                $result->canIndexMore = false;
                break;
            }
        }
        
        /**
         * Convert pages to post data
         */
         foreach ($pages as $page) {
             foreach ($page->children as $child) {
                 $postData = $this->convertRedditPostToPostData($child->data);
                 if ($postData) {
                     $result->addPostData($postData);
                 }
             }
         }
         
        /**
         * Store last few posts for resuming later.
         */
        $afterPosts = [];
        $count = count($result->postData);
        for ($i = 0; $i < $count; $i++) {
            if ($i > self::RESUME_POSTS_COUNT) {
                break;
            }
            $afterPosts[] = $result->postData[$i]->identifier;
        }
        
        if (count($afterPosts) < self::RESUME_POSTS_COUNT) {
            foreach ($this->afterPosts as $afterPost) {
                $afterPosts[] = $afterPost;
            }
        }
         
        /**
         * Create result
         */
        $result->setResumeInfo([
            'after_posts' => $afterPosts,
        ]);
        $result->success = true;
        return $result;
    }
    
    /**
     * Converts a reddit post object into a PostData object.
     * @param stdClass $redditPost Reddit post object
     * @return PostData
     */
    private function convertRedditPosttoPostData($redditPost) {
        if (!property_exists($redditPost, 'permalink')) {
            return null;
        }
        if (strpos($redditPost->url, 'reddit.com/r/') !== FALSE) {
            return null;
        }
        
        $postData = new PostData;
        $postData->sourceConfig = $this->config;
        
        $postData->identifier = 't3_' . $redditPost->id;
        $postData->permalink = $redditPost->permalink;
        $postData->postedAt = Carbon::createFromTimestamp($redditPost->created_utc);
        
        $postData->title = $redditPost->title;
        if ($redditPost->author != '[deleted]') {
            $postData->authorName = $redditPost->author;
            $postData->authorIdentifier = $redditPost->author_fullname;
        }
        
        $postData->mediaUrl = $redditPost->url;
        
        if ($redditPost->media) {
            $postData->width = $redditPost->media->oembed->width ?? null;
            $postData->height = $redditPost->media->oembed->height ?? null;
            $postData->thumbnailWidth = $redditPost->media->oembed->thumbnail_width ?? null;
            $postData->thumbnailHeight = $redditPost->media->oembed->thumbnail_height ?? null;
            $postData->thumbnailUrl = $redditPost->media->oembed->thumbnail_url ?? null;
        }
        
        if (property_exists($redditPost, 'post_hint')) {
            $postData->sourceMediaTypeHint = $redditPost->post_hint;
        }
        
        return $postData;
    }
    
    /**
     * Grabs a single page from reddit.
     * Will return min($limit, MAX_PAGE_SIZE, number of posts) posts.
     * @return stdClass|null Reddit page object
     */
    private function indexPage($beforeFullname, $limit) {
        $limit = min($limit, self::MAX_PAGE_SIZE);
        
        $pageData = $this->reddit->indexNew($this->subredditSlug, [
            'before' => $beforeFullname,
            'limit' => $limit,
            'show' => 'all',
        ]);
        
        return $pageData;
    }
    
    /**
     * Attempts to find the last post we indexed which still exists on Reddit.
     * @return stdClass|null reddit post object or null if all removed
     */
    private function findLastExistingPost() {
        foreach ($this->afterPosts as $fullname) {
            $postData = $this->reddit->viewPost($fullname);
            if ($postData) {
                return $postData;
            }
        }
        return null;
    }
}