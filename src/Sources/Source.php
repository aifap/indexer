<?php
namespace AIFap\Indexer\Sources;

use AIFap\Indexer\Data\SourceConfig;

/**
 * Encapsulates indexing methods for a given content source or strategy.
 */
abstract class Source {
    protected $config;
    
    public function __construct(SourceConfig $config) {
        $this->config = $config;
    }
    
    public abstract function setup();
}