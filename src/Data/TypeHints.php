<?php
namespace AIFap\Indexer\Data;

/**
 * Type hinting information which improves the indexer's guessing of post types.
 */
class TypeHints {
    /**
     * Gender type hints
     */
     
    public $fixedGenderType = null;
    public $alwaysSolo = false;
    public $neverSolo = false;
    public $alwaysInvolveWomen = false;
    public $neverInvolveWomen = false;
    public $alwaysInvolveMen = false;
    public $neverInvolveMen = false;
    public $alwaysStraight = false;
    public $neverStraight = false;
    public $alwaysGay = false;
    public $neverGay = false;
    public $alwaysLesbian = false;
    public $neverLesbian = false;
    public $alwaysTransexual = false;
    public $neverTransexual = false;
    
    
    /**
     * Artystyle type hints
     */
     
    public $fixedArtstyleType = false;
    public $allowsReallife = false;
    public $bansReallife = false;
    public $allowsHentai = false;
    public $bansHentai = false;
    public $allows3d = false;
    public $bans3D = false;
    
    public function __construct($data = []) {
        foreach ($data as $key => $value) {
            $key = static::toCamelCase($key);
            $this->{$key} = $value;
        }
    }
    
    private static function toCamelCase($string) {
        $value = ucwords(str_replace(['-', '_'], ' ', $string));
        return lcfirst(str_replace(' ', '', $value));
    }
}