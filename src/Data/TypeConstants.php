<?php
namespace AIFap\Indexer\Data;

/**
 * These are the named constants for post types.
 * 
 * Posts have three types:
 * - gender-type: the orientation of the post
 * - media-type: the type of file
 * - artstyle-type: how the post was created
 */
class TypeConstants {
    /** Post contains only a single woman */
    const GENDER_SOLO_WOMEN = 'solo-women';
    /** Post contains only a single man */
    const GENDER_SOLO_MEN = 'solo-men';
    /** Post contains straight sex, or a mix of men and women */
    const GENDER_STRAIGHT = 'straight';
    /** Post contains a group of men */
    const GENDER_GAY = 'gay';
    /** Post contains a group of women */
    const GENDER_LESBIAN = 'lesbian';
    /** Post contains any non-binary individual */
    const GENDER_TRANSEXUAL = 'transexual';
    /** Post orientation can't be identified */
    const GENDER_UNKNOWN = 'unknown';
    
    /** Post is a static image (i.e. JPG, PNG) */
    const MEDIA_IMAGE = 'image';
    /** Post is an animated GIF or direct video link (i.e. WebM, MP4) */
    const MEDIA_ANIMATED = 'animated';
    /** Post is a video which requires a third-party embed (i.e. Pornhub) */
    const MEDIA_VIDEO = 'video';
    /** Post is an audio file (i.e. MP3) */
    const MEDIA_AUDIO = 'audio';
    /** Post is an audio file which requires a thrird-party embed */
    const MEDIA_AUDIO_EMBED = 'audio-embed';
    /** Post is textual content */
    const MEDIA_TEXT = 'text';
    /** Post is a link which can't be embedded */
    const MEDIA_LINK = 'link';
    /** Post media type can't be determined */
    const MEDIA_UNKNOWN = 'unknown';
    
    /** Post is a real-life recording or photo */
    const ARTSTYLE_REALLIFE = 'real-life';
    /** Post is a hand- or digitally-drawn image or animation */
    const ARTSTYLE_HENTAI = 'hentai';
    /** Post is a CG image or animation */
    const ARTSTYLE_3D = '3d';
    /** Post artstyle can't be determined */
    const ARTSTYLE_UNKNOWN = 'unknown';
   
    /** Array containing all possible gender-types */
    const ALL_GENDER = [
        self::GENDER_SOLO_WOMEN,
        self::GENDER_SOLO_MEN,
        self::GENDER_STRAIGHT,
        self::GENDER_GAY,
        self::GENDER_LESBIAN,
        self::GENDER_TRANSEXUAL,
        self::GENDER_UNKNOWN,
    ];
    
    /** Array containing all possible media-types */
    const ALL_MEDIA = [
        self::MEDIA_IMAGE,
        self::MEDIA_ANIMATED,
        self::MEDIA_VIDEO,
        self::MEDIA_AUDIO,
        self::MEDIA_AUDIO_EMBED,
        self::MEDIA_TEXT,
        self::MEDIA_LINK,
        self::MEDIA_UNKNOWN,
    ];
    
    /** Array containing all possible artstyle-types */
    const ALL_ARTSTYLE = [
        self::ARTSTYLE_REALLIFE,
        self::ARTSTYLE_HENTAI,
        self::ARTSTYLE_3D,
        self::ARTSTYLE_UNKNOWN,
    ];
        
}
    