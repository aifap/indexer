<?php
namespace AIFap\Indexer\Data;

use AIFap\Indexer\Data\TypeConstants;

/**
 * Represents a post that has been indexed.
 */
class PostData {
    public $identifier;
    public $permalink;
    public $title;
    public $authorIdentifier;
    public $authorName;
    public $postedAt;
    
    public $mediaUrl;
    
    public $width = null;
    public $height = null;
    public $thumbnailWidth = null;
    public $thumbnailHeight = null;
    public $thumbnailUrl = null;
    public $typeGender = TypeConstants::GENDER_UNKNOWN;
    public $typeMedia = TypeConstants::MEDIA_UNKNOWN;
    public $typeArtstyle = TypeConstants::ARTSTYLE_UNKNOWN;
    
    public $sourceConfig;
    
    public $sourceMediaTypeHint = null;
    
    public function __construct() {
    }
    
    public static function fromRedditPost($redditPost) {
    }
}