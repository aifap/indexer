<?php
namespace AIFap\Indexer\Data;

use AIFap\Indexer\Data\SourceConfig;
use AIFap\Indexer\Data\PostData;

/**
 * Represents the results of an indexing run.
 */
class IndexingResult {
    public $postData = [];
    public $postCount = 0;
    
    public $sourceConfig;
    public $resumeInfo = [];
    
    public $success = false;
    public $canIndexMore = false;
    public $warnings = [];
    
    public function __construct(SourceConfig $config) {
        $this->sourceConfig = $config;
    }
    
    public function addWarning($message) {
        $this->warnings[] = $message;
    }
    
    public function addPostData(PostData $postData) {
        $this->postData[] = $postData;
        $this->postCount++;
    }
    
    public function setResumeInfo($info) {
        foreach ($info as $key => $value) {
            $this->resumeInfo[$key] = $value;
        }
    }
    
}