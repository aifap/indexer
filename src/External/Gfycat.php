<?php
namespace AIFap\Indexer\External;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;

/**
 * Encapsulates services provided by Gfycat.
 */
class Gfycat {
    protected $client;
    
    protected $clientId;
    protected $clientSecret;
    protected $accessToken;
    
    public function __construct($clientId, $clientSecret) {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        
        $this->makeClient();
        $this->getAccessToken();
    }
    
    private function makeClient($accessToken = null) {
        $headers = [];
        if ($accessToken) {
            $headers['Authorization'] = 'Bearer ' . $accessToken;
        }
        $this->client = new GuzzleClient([
            'base_uri' => 'https://api.gfycat.com/v1/',
            'timeout'  => 10.0,
            'headers' => $headers,
        ]);
    }
    
    /**
     * Retrieves a fresh OAuth access token from Gfycat.
     * https://developers.gfycat.com/api/#authentication
     */
    private function getAccessToken() {
        $res = $this->client->request('POST', 'oauth/token', [
            'json' => [
                'grant_type' => 'client_credentials',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
            ],
        ]);
        $data = json_decode($res->getBody()); 
        $this->accessToken = $data->access_token;
        
        // Include token with future requests
        $this->makeClient($this->accessToken);
    }
    
    /**
     * Retrieves details for a single gfycat.
     * https://developers.gfycat.com/api/#getting-info-for-a-single-gfycat
     * @return stdClass gfyItem object or null if 404
     */
    public function viewGfycat($gfyId) {
        $attempts = 0;
        while ($attempts < 5) {
            $attempts++;
            try {
                $res = $this->client->request('GET', 'gfycats/' . $gfyId);
                $data = json_decode($res->getBody());
                return $data->gfyItem;
            } catch (RequestException $ex) {
                $code = $ex->getCode();
                if ($code == 429) { // Too Many Requests
                    // Wait and try again
                    sleep(5);
                    continue;
                }
                if ($code == 404) { // Not Found
                    return null;
                }
                // TODO
                throw $ex;
            }
        }
    }
}