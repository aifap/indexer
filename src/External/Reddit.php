<?php
namespace AIFap\Indexer\External;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;

/**
 * Encapsulates services provided by Gfycat.
 */
class Reddit {
    protected $client;
    
    protected $userAgent;
    
    public function __construct($userAgent) {
        $this->userAgent = $userAgent;
        $this->makeClient($this->userAgent);
    }
    
    private function makeClient($userAgent) {
        $this->client = new GuzzleClient([
            'base_uri' => 'https://reddit.com/',
            'timeout'  => 10.0,
            'headers' => [
                'User-Agent' => $userAgent,
            ],
        ]);
    }
    
    /**
     * Retrieves details for a single post.
     * https://www.reddit.com/dev/api/#GET_by_id_{names}
     * @return stdClass|null reddit post object or null if 404
     */
    public function viewPost($fullname) {
        $attempts = 0;
        while ($attempts < 5) {
            $attempts++;
            try {
                $res = $this->client->request('GET', 'by_id/' . $fullname . '.json');
                $data = json_decode($res->getBody());
                $children = $data->data->children;
                if (count($children) > 1) {
                    return null;
                }
                return $children[0]->data;
            } catch (RequestException $ex) {
                $code = $ex->getCode();
                if ($code == 429) { // Too Many Requests
                    // Wait and try again
                    sleep(5);
                    continue;
                }
                if ($code == 404) { // Not Found
                    return null;
                }
                // TODO
                throw $ex;
            }
        }
    }
    
    /**
     * Retrieves a page from the new listing.
     * https://www.reddit.com/dev/api/#GET_new
     * @return stdClass|null reddit page object or null
     */
    public function indexNew($subreddit = null, $options = []) {
        $attempts = 0;
        while ($attempts < 5) {
            $attempts++;
            try {
                $url = $subreddit ? 'r/' . $subreddit . '/new.json' : 'new.json';
                $res = $this->client->request('GET', $url, [
                    'query' => $options,
                ]);
                $data = json_decode($res->getBody());
                if (count($data->data->children) < 1) {
                    return null;
                }
                return $data->data;
            } catch (RequestException $ex) {
                $code = $ex->getCode();
                if ($code == 429) { // Too Many Requests
                    // Wait and try again
                    sleep(5);
                    continue;
                }
                if ($code == 404) { // Not Found
                    return null;
                }
                // TODO
                throw $ex;
            }
        }
    }
}