<?php
namespace AIFap\Indexer\Processing;

use AIFap\Indexer\Processing\PostProcessor;

use AIFap\Indexer\Categorization\ArtstyleTypeGuesser;
use AIFap\Indexer\Categorization\MediaTypeGuesser;
use AIFap\Indexer\Categorization\GenderTypeGuesser;
use AIFap\Indexer\Data\TypeConstants;

/**
 * Runs type guessers against posts that have unknown types.
 */
class TypeGuessing extends PostProcessor {
    public function run(array $postDataArray) {
        $artstyleTypeGuesser = new ArtstyleTypeGuesser;
        $mediaTypeGuesser = new MediaTypeGuesser;
        $genderTypeGuesser = new GenderTypeGuesser;
        
        foreach ($postDataArray as $postData) {
            if ($postData->typeGender == TypeConstants::GENDER_UNKNOWN) {
                $postData->typeGender = $genderTypeGuesser->guess($postData, $this->config);
            }
            if ($postData->typeArtstyle == TypeConstants::ARTSTYLE_UNKNOWN) {
                $postData->typeArtstyle = $artstyleTypeGuesser->guess($postData, $this->config);
            }
            if ($postData->typeMedia == TypeConstants::MEDIA_UNKNOWN) {
                $postData->typeMedia = $mediaTypeGuesser->guess($postData, $this->config);
            }
        }
        return $postDataArray;
    }
}