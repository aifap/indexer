<?php
namespace AIFap\Indexer\Processing;

use AIFap\Indexer\Processing\PostProcessor;

use AIFap\Indexer\External\Gfycat;

/**
 * Cleans up Gfycat URLs on posts.
 * 
 * Gfycat posts are often shared in formats that don't easily map to the
 * final media file. For example, the Gfycat post ID can be shared in lowercase,
 * but the final media file ID must be in the exact original casing.
 * 
 * Uses the Gfycat API to retrieve the correct details.
 */
class GfycatUrlFixer extends PostProcessor {
    public function run(array $postDataArray) {
        $gfy = new Gfycat(
            $this->config->getSourceInfo('gfycat_client_id', ['required' => true]),
            $this->config->getSourceInfo('gfycat_client_secret', ['required' => true])
        );
        
        foreach ($postDataArray as $postData) {
            if (preg_match('/^https?:\/\/(?:.*\.)?gfycat\.com\/(?:.*\/)?(.*?)(?:-.*)?(?:\..*)?$/m', $postData->mediaUrl, $matches)) {
                $gfyId = $matches[1];
                
                $gfyItem = $gfy->viewGfycat($gfyId);
                if ($gfyItem) {
                    // Found gfycat, use correct media urls
                    $postData->mediaUrl = $gfyItem->webmUrl;
                    $postData->thumnailUrl = $gfyItem->gif100px;
                }
            }
        }
        return $postDataArray;
    }
}