We welcome contributions in the form of bug fixes,  new sources,  and improved
post processing.

### Issue tracking
Please use [aifap/aifap-issues](https://gitlab.com/aifap/aifap-issues) for issues that affect the AIFap site. The issue
tracker on this project is exclusively for issues related to the use of this 
standalone library.

### Adding new sources
You can add new sources by extending the Source class. If you're experimenting
with a new source and think it'd be a good fit for AIFap, send us a PR and we'll
be happy to take a look.

We can't guarentee that we'll add support for your source to AIFap if you 
submit support for it in a PR. There's a number of things we'd have to consider 
before accepting additional sources.

### Adding new processing steps
You can add new processing steps to clean up posts by extending the PostProcessor
class. PRs implementing new processors which clean up common issues or which can 
be used to avoid human categorization are very welcome.

### Using this indexer for other projects
Please feel free to use this project in other contexts (within the terms of the
MPLv2). Remember to share any changes you make, and please remember to provide
attribution to this project on Gitlab.